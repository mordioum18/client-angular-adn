import {Component, OnInit} from '@angular/core';
import {Article} from './model/article';
import {ArticleService} from './service/article.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ArticleService],
})
export class AppComponent{

}
