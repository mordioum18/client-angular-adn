import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {APPLICATION_ID, BASE_URL_LOGIN, BASE_URL_LOGOUT, REST_API_KEY} from '../../assets/constante';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(private http: HttpClient) { }

  login(username: string, password: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Parse-Application-Id':  APPLICATION_ID,
        'X-Parse-REST-API-Key': REST_API_KEY,
        'Content-Type': 'Application/json',
        'X-Parse-Revocable-Session': '1'
      }),
      params: new HttpParams().set('username', username).set('password', password)
    };
    return this.http.get(BASE_URL_LOGIN, httpOptions);
  }

  logout(token: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Parse-Application-Id':  APPLICATION_ID,
        'X-Parse-REST-API-Key': REST_API_KEY,
        'X-Parse-Session-Token': token
      })
    };
    return this.http.post(BASE_URL_LOGOUT,{}, httpOptions);
  }
}
