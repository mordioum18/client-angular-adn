import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams,} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../model/user';
import {APPLICATION_ID, BASE_URL_USER, REST_API_KEY} from '../../assets/constante';

@Injectable()
export class UserService {

  constructor(private http: HttpClient){
  }

  post(user: User): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Parse-Application-Id':  APPLICATION_ID,
        'X-Parse-REST-API-Key': REST_API_KEY,
        'Content-Type': 'Application/json',
        'X-Parse-Revocable-Session': '1'
      })
    };
    return this.http.post(BASE_URL_USER , user, httpOptions);
  }

  put(user: User): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Parse-Application-Id':  APPLICATION_ID,
        'X-Parse-REST-API-Key': REST_API_KEY,
        'Content-Type': 'Application/json',
      })
    };
    return this.http.put(BASE_URL_USER + '/' + user.objectId, user, httpOptions);
  }

  delete(userId: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Parse-Application-Id':  APPLICATION_ID,
        'X-Parse-REST-API-Key': REST_API_KEY,
        'Content-Type': 'Application/json',
      })
    };
    return this.http.delete(BASE_URL_USER + '/' + userId, httpOptions);
  }

  getAll(): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Parse-Application-Id':  APPLICATION_ID,
        'X-Parse-REST-API-Key': REST_API_KEY,
        'Content-Type': 'Application/json',
      })
    };
    return this.http.get(BASE_URL_USER, httpOptions);
  }
  getById(userId: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Parse-Application-Id':  APPLICATION_ID,
        'X-Parse-REST-API-Key': REST_API_KEY,
        'Content-Type': 'Application/json',
      })
    };
    return this.http.get(BASE_URL_USER + '/' + userId, httpOptions);
  }

  getByToken(token: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Parse-Application-Id':  APPLICATION_ID,
        'X-Parse-REST-API-Key': REST_API_KEY,
        'X-Parse-Session-Token': token,
      })
    };
    return this.http.get(BASE_URL_USER + '/me', httpOptions);
  }
}
