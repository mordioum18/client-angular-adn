import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders,} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Article} from '../model/article';
import {APPLICATION_ID, BASE_URL, REST_API_KEY} from '../../assets/constante';

@Injectable()
export class ArticleService {

  constructor(private http: HttpClient){
  }

  post(article: Article): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Parse-Application-Id':  APPLICATION_ID,
        'X-Parse-REST-API-Key': REST_API_KEY,
        'Content-Type': 'Application/json',
      })
    };
    return this.http.post(BASE_URL + 'Article', article, httpOptions);
  }

  put(article: Article): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Parse-Application-Id':  APPLICATION_ID,
        'X-Parse-REST-API-Key': REST_API_KEY,
        'Content-Type': 'Application/json',
      })
    };
    return this.http.put(BASE_URL + 'Article/' + article.objectId, article, httpOptions);
  }

  delete(articleId: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Parse-Application-Id':  APPLICATION_ID,
        'X-Parse-REST-API-Key': REST_API_KEY,
        'Content-Type': 'Application/json',
      })
    };
    return this.http.delete(BASE_URL + 'Article/' + articleId, httpOptions);
  }

  getAll(): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Parse-Application-Id':  APPLICATION_ID,
        'X-Parse-REST-API-Key': REST_API_KEY,
        'Content-Type': 'Application/json',
      })
    };
    return this.http.get(BASE_URL + 'Article', httpOptions);
  }
  getById(articleId: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Parse-Application-Id':  APPLICATION_ID,
        'X-Parse-REST-API-Key': REST_API_KEY,
        'Content-Type': 'Application/json',
      })
    };
    return this.http.get(BASE_URL + 'Article/' + articleId, httpOptions);
  }
}
