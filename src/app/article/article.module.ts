import { NgModule } from '@angular/core';
import {ArticleRoutingModule} from './article-routing.module';
import {FormArticleComponent} from './form-article/form.article.component';
import {ArticleComponent} from './article.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ArticleService} from '../service/article.service';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    FormArticleComponent,
    ArticleComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ArticleRoutingModule
  ],
  providers: [ArticleService],
})
export class ArticleModule { }
