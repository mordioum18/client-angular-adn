import {Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Article} from '../../model/article';
import {ArticleService} from '../../service/article.service';

@Component({
  selector: 'app-form-article',
  templateUrl: './form.article.component.html',
  styleUrls: ['./form.article.component.scss'],
  providers: [ArticleService],
})

export class FormArticleComponent implements OnInit, OnChanges {
  @Input() articleSelected: Article ;
  @Output() retourArticleId: EventEmitter<string> = new EventEmitter();
  formArticle: FormGroup;
  constructor(private fb: FormBuilder,
              private articleService: ArticleService) {
  }
  ngOnInit(): void {
    this.formArticle = this.fb.group({
      titre: [null, [Validators.required, Validators.maxLength(50)]],
      description: [null, [Validators.required, Validators.maxLength(255)]],
      estPublie: [false]
    });
  }
  onSubmit(){
    const article = new Article(this.formArticle.get('titre').value, this.formArticle.get('description').value, this.formArticle.get('estPublie').value);
    if(this.articleSelected && this.articleSelected.objectId){
      article.objectId = this.articleSelected.objectId;
      this.articleService.put(article)
        .subscribe(
          value => {
            this.retourArticleId.emit(this.articleSelected.objectId);
            this.formArticle.reset();
          }
        );
    }
    else {
      this.articleService.post(article)
        .subscribe(
          value => {
            console.log(value)
            this.retourArticleId.emit(value.objectId);
            this.formArticle.reset();
          }
        );
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.articleSelected !== undefined){
      this.formArticle.get('titre').setValue(this.articleSelected.titre);
      this.formArticle.get('description').setValue(this.articleSelected.description);
      this.formArticle.get('estPublie').setValue(this.articleSelected.estPublie);
    }
  }
}
