import { Component, OnInit } from '@angular/core';
import {Article} from '../model/article';
import {ArticleService} from '../service/article.service';

import Swal from 'sweetalert2';
import {UserService} from '../service/user.service';
import {Router} from '@angular/router';
import {LogService} from '../service/log.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
  providers: [UserService, LogService]
})
export class ArticleComponent implements OnInit {

  articles: Article[] = [];
  articleSelected: Article;
  constructor(private articleService: ArticleService,
              private userService: UserService,
              private logService: LogService,
              private router: Router){
  }
  ngOnInit(): void {
    const session = JSON.parse(localStorage.getItem('session'));
    if (session !==  undefined && session !== null && session.sessionToken !== undefined){
      this.userService.getByToken(session.sessionToken)
        .subscribe(
          value => this.getArticles(),
          error1 => this.router.navigate(['/'])
        );
    }
    else {
      this.router.navigate(['/']);
    }
  }
  getArticles(){
    this.articleService.getAll()
      .subscribe(value => this.articles = value.results);
  }
  addArticle(articleId){
    this.articleService.getById(articleId)
      .subscribe(
        value => {
          for (let i= 0; i < this.articles.length; i++){
            if (this.articles[i].objectId === value.objectId){
              this.articles.splice(i, 1);
              this.articles.splice(i, 0, value);
              return;
            }
          }
          this.articles.push(value);
        }
      );
  }

  onEdit(article: Article){
    this.articleSelected = article;
  }

  onDelete(article: Article){
    Swal.fire({
      title: 'Êtes vous sûr?',
      text: 'Vous êtes sur le point de supprimer ' + article.titre,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Supprimer',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.articleService.delete(article.objectId)
          .subscribe(
            value => {
              for (let i = 0; i < this.articles.length; i++){
                if (this.articles[i].objectId === article.objectId){
                  this.articles.splice(i, 1);
                  break;
                }
              }
              Swal.fire(
                'Suppriomé!',
                'Vous venez de supprimer l \'article',
                'success'
              );
            }
          );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }

  onDisconnect(){
    const session = JSON.parse(localStorage.getItem('session'));
    if (session !==  undefined && session !== null && session.sessionToken !== undefined){
      this.logService.logout(session.sessionToken)
        .subscribe(
          value => {
            localStorage.clear();
            this.router.navigate(['/']);
          },
          error1 => console.log(error1)
        );
    }
  }

}
