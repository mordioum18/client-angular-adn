import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: 'articles',  loadChildren: './article/article.module#ArticleModule'},
  {path: 'users',  loadChildren: './user/user.module#UserModule'},
  {path: '',  loadChildren: './user/user.module#UserModule'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
