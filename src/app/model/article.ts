export class Article {
  objectId: string;
  titre: string;
  description: string;
  estPublie: boolean;
  constructor(titre: string, description: string, estPublie: boolean) {
    this.titre = titre;
    this.description = description;
    this.estPublie = estPublie;
  }
}
