import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LogService} from '../../service/log.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-form-user',
  templateUrl: './form.login.component.html',
  styleUrls: ['./form.login.component.scss'],
  providers: [LogService],
})

export class FormLoginComponent implements OnInit {
  formLogin: FormGroup;
  isSubmit = false;
  errorMessage: string;
  constructor(private fb: FormBuilder,
              private logService: LogService,
              private router: Router) {
  }
  ngOnInit(): void {
    this.formLogin = this.fb.group({
      email: [null, [Validators.required, Validators.email, Validators.maxLength(50)]],
      password: [null, [Validators.required, Validators.maxLength(30)]]
    });
  }
  onSubmit() {
    this.errorMessage = undefined;
    this.isSubmit = true;
    this.logService.login(this.formLogin.get('email').value, this.formLogin.get('password').value)
      .subscribe(
        val => {
          console.log(val);
          localStorage.setItem('session', JSON.stringify(val));
          this.isSubmit = false;
          this.router.navigate(['/articles'])
        },
        error1 => {
          this.isSubmit = false;
          this.errorMessage = 'Combinaison incorrecte';
        }
      );
  }
}
