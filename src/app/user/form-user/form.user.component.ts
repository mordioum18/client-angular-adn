import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../model/user';
import {UserService} from '../../service/user.service';

@Component({
  selector: 'app-form-user',
  templateUrl: './form.user.component.html',
  styleUrls: ['./form.user.component.scss'],
  providers: [UserService],
})

export class FormUserComponent implements OnInit {
  formUser: FormGroup;
  isSubmit = false;
  isRegistered = false;
  errorMessage: string;
  constructor(private fb: FormBuilder,
              private userService: UserService) {
  }
  ngOnInit(): void {
    this.formUser = this.fb.group({
      email: [null, [Validators.required, Validators.email, Validators.maxLength(50)]],
      password: [null, [Validators.required, Validators.maxLength(30)]],
    });
  }
  onSubmit() {
    this.errorMessage = undefined;
    const user = new User(this.formUser.get('email').value, this.formUser.get('email').value, this.formUser.get('password').value);
    this.isSubmit = true;
    this.userService.post(user)
      .subscribe(
        val => {
          console.log(val);
          this.isSubmit = false;
          this.isRegistered = true;
        },
        error1 => {
          this.errorMessage = 'Un compte avec ce email existe déjà';
          this.isSubmit = false;
        }
      );
  }
}
