import { Component, OnInit } from '@angular/core';
import {UserService} from '../service/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  providers: [UserService]
})
export class UserComponent implements OnInit {

  constructor(private userService: UserService,
              private router: Router) { }

  ngOnInit() {
    const session = JSON.parse(localStorage.getItem('session'));
    if (session !== undefined && session !== null &&  session.sessionToken !== undefined){
      this.userService.getByToken(session.sessionToken)
        .subscribe(
          value => this.router.navigate(['/articles'])
        );
    }
  }

}
