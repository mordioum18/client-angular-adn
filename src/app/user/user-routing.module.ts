import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserComponent} from './user.component';
import {FormUserComponent} from './form-user/form.user.component';
import {FormLoginComponent} from './form-login/form.login.component';

const routes: Routes = [
  {
    path: '', component: UserComponent,
    children: [
      {path: '', component: FormLoginComponent},
      {path: 'register', component: FormUserComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
