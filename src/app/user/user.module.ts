import { NgModule } from '@angular/core';
import {UserRoutingModule} from './user-routing.module';
import {FormUserComponent} from './form-user/form.user.component';
import {UserComponent} from './user.component';
import {ReactiveFormsModule} from '@angular/forms';
import {UserService} from '../service/user.service';
import {CommonModule} from '@angular/common';
import {FormLoginComponent} from './form-login/form.login.component';

@NgModule({
  declarations: [
    FormUserComponent,
    UserComponent,
    FormLoginComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    UserRoutingModule
  ],
  providers: [UserService],
})
export class UserModule { }
